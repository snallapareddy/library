package com.nreddy.restapi;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.nreddy.model.Book;
import com.nreddy.model.PrintedBook;

@Path("/catalog")
public class LibraryRest {

	// This method is called if TEXT_PLAIN is request
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Book getBook() {
		return new PrintedBook("name", "9", "2");
	}
}

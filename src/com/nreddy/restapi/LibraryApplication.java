package com.nreddy.restapi;

import org.glassfish.jersey.server.ResourceConfig;

public class LibraryApplication extends ResourceConfig{
	public LibraryApplication() {
        packages("com.fasterxml.jackson.jaxrs.json","com.nreddy.restapi");
    }
}

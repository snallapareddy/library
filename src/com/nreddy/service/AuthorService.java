
package com.nreddy.service;

import com.nreddy.model.Author;

public interface AuthorService {

	/**
	 * Finds author by id
	 * 
	 * @param id
	 *            takes in id
	 * @return returns author object
	 */
	Author findById(String id);

	/**
	 * adds author to list
	 * 
	 * @param a
	 *            takes in author to be added
	 */
	void addAuthor(Author a);

	/**
	 * removes certain author from list
	 * 
	 * @param a
	 *            takes in author object to be removed
	 */
	void removeAuthor(Author a);
}
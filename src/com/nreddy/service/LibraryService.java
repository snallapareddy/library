package com.nreddy.service;

import java.util.List;

import com.nreddy.model.Book;

/**
 * This class is meant to catalog the books in a library
 * 
 * @author sahith.nallapareddy
 * 
 */
public interface LibraryService {
	/**
	 * Adds a book into library; if book already exists, the library will get
	 * updated
	 * 
	 * @param b
	 *            Take a book as an input
	 */
	void addBook(Book b);

	/**
	 * Removes a certain book from the library; if book does not exists, then
	 * nothing will be removed
	 * 
	 * @param b
	 *            Takes book as an input
	 */
	void removeBook(Book b);

	/**
	 * Finds a certain book by the ISBN; If book is not there, return null
	 * 
	 * @param ISBN
	 *            takes in a String of ISBN
	 * @return returns a book object
	 */
	Book findByIsbn(String ISBN);

	/**
	 * Finds all books written by certain author; If no book is found, returns
	 * null
	 * 
	 * @param Author
	 *            takes in an Author object
	 * @return returns a list of book objects
	 */
	List<Book> findByAuthor(String id);

	/**
	 * Returns all books
	 * 
	 * @return returns a list of all books
	 */
	List<Book> findAllBooks();
}

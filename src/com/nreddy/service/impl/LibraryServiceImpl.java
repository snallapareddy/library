package com.nreddy.service.impl;

import java.util.ArrayList;
import java.util.List;

import com.nreddy.model.Book;
import com.nreddy.service.LibraryService;

public class LibraryServiceImpl implements LibraryService {

	private static ArrayList<Book> books = new ArrayList<Book>();

	public void addBook(Book b) {
		books.add(b);
	}

	public void removeBook(Book b) {
		for (int i = 0; i < books.size(); i++) {
			if (books.get(i).getISBN() == b.getISBN()) {
				books.remove(i);
				i--;
			}
		}
	}

	@Override
	public Book findByIsbn(String ISBN) {
		for (int i = 0; i < books.size(); i++) {
			if (books.get(i).getISBN() == ISBN) {
				return books.get(i);
			}
		}
		return null;
	}

	@Override
	public List<Book> findByAuthor(String id) {
		ArrayList<Book> booksByAuthor = new ArrayList<Book>();
		for (int i = 0; i < books.size(); i++) {
			if (books.get(i).getAuthorId().equals(id)) {
				booksByAuthor.add(books.get(i));
			}
		}
		return booksByAuthor;
	}

	@Override
	public List<Book> findAllBooks() {

		return books;
	}

}

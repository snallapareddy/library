package com.nreddy.service.impl;

import java.util.ArrayList;

import com.nreddy.model.Author;
import com.nreddy.service.AuthorService;

public class AuthorServiceImpl implements AuthorService {

	private static ArrayList<Author> authors = new ArrayList<Author>();

	@Override
	public Author findById(String id) {
		for (int i = 0; i < authors.size(); i++) {
			if(authors.get(i).getId().equals(id)){
				return authors.get(i);
			}
		}
		return null;
	}

	@Override
	public void addAuthor(Author a) {
		authors.add(a);

	}

	@Override
	public void removeAuthor(Author a) {
		if(getIndexOfAuthor(a) == -1){
			return;
		}
		authors.remove(getIndexOfAuthor(a));

	}
	
	private static int getIndexOfAuthor(Author a){
		for(int i = 0;i<authors.size();i++){
			if(authors.get(i).getId() == a.getId()){
				return i;
			}
		}
		return -1;
	}

}

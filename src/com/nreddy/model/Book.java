package com.nreddy.model;

import java.util.Date;

public abstract class Book {
	
	private String _title;
	private String _authorId;
	private String _imgURL;
	private Date _releaseDate;
	private String _ISBN;
	
	public Book(String name, String id, String ISBN){
		this._title = name;
		this._authorId = id;
		this._ISBN = ISBN;
	}
	public String getTitle(){
		return _title;
	}
	
	public String getISBN() {
		return _ISBN;
	}
	public void setISBN(String ISBN) {
		this._ISBN = ISBN;
	}
	public Date getReleaseDate() {
		return _releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this._releaseDate = releaseDate;
	}
	public String getImgURL() {
		return _imgURL;
	}
	public String getAuthorId(){
		return _authorId;
	}
	
	public String toString(){
		String book = "";
		book = this._title + " by " + _authorId;
		return book;
	}

}

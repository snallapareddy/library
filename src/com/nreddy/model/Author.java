package com.nreddy.model;

import java.util.UUID;

public class Author {
	private String firstName;
	private String lastName;
	private String id;

	public Author(String fName, String lName) {
		firstName = fName;
		lastName = lName;
		id = UUID.randomUUID().toString();
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public String getId() {
		return id;
	}

}
